﻿using UnityEngine;
using System.Collections;

public enum PlayerIdentifier
{
    P1, P2, P3, P4, AI
}

public class ControllerManager : MonoBehaviour
{


    public PlayerIdentifier assignedPlayer = PlayerIdentifier.P1;
    public float sensibility = 120.0f;

    //button strings
    [HideInInspector]
    public string sWalkX;
    [HideInInspector]
    public string sWalkY;
    [HideInInspector]
    public string sJump;
    [HideInInspector]
    public string sFire;
    [HideInInspector]
    public string sUse;
    [HideInInspector]
    public string sStart;
    [HideInInspector]
    public string sSelect;

    //button status
    [HideInInspector]
    public bool bJumpPressed;
    [HideInInspector]
    public bool bJumpDown;
    [HideInInspector]
    public bool bJumpReleased;
    [HideInInspector]
    public bool bFirePressed;
    [HideInInspector]
    public bool bFireHeld;
    [HideInInspector]
    public bool bUsePressed;
    [HideInInspector]
    public bool bStartPressed;
    [HideInInspector]
    public bool bSelectPressed;

    [HideInInspector]
    public float fWalkXInput;
    [HideInInspector]
    public float fWalkYInput;
    [HideInInspector]
    public float fLookXInput;
    [HideInInspector]
    public float fLookYInput;

    [HideInInspector]
    public bool bLeftPressed;
    [HideInInspector]
    public bool bRightPressed;
    [HideInInspector]
    public bool bUpPressed;
    [HideInInspector]
    public bool bDownPressed;



    // Use this for initialization
    public void Start()
    {
        //TODO: Set strings for the desired player based on P1, P2 etc
        sWalkX = assignedPlayer + " Horizontal";
        sWalkY = assignedPlayer + " Vertical";
        sJump = assignedPlayer + " Jump";
        sFire = assignedPlayer + " Attack";
        sUse = assignedPlayer + " Use";
        sStart = assignedPlayer + " Start";
        sSelect = assignedPlayer + " Select";
    }

    public void UpdateAssignedController (PlayerIdentifier _targetPlayer)
    {
        assignedPlayer = _targetPlayer;
        UpdateStrings();
    }

    void UpdateStrings ()
    {
        sWalkX = assignedPlayer + " Horizontal";
        sWalkY = assignedPlayer + " Vertical";
        sJump = assignedPlayer + " Jump";
        sFire = assignedPlayer + " Attack";
        sUse = assignedPlayer + " Use";
        sStart = assignedPlayer + " Start";
        sSelect = assignedPlayer + " Select";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
