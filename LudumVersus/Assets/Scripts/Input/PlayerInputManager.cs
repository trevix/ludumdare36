﻿using UnityEngine;
using System.Collections;

public class PlayerInputManager : ControllerManager {

    // Use this for initialization
    void Start ( ) {
        base.Start();
    }

    // Update is called once per frame
    void Update ( ) {
        if (assignedPlayer == PlayerIdentifier.AI) {
            return;
        }
        bJumpDown = Input.GetButton(sJump);
        bJumpPressed = Input.GetButtonDown(sJump);
        bJumpReleased = Input.GetButtonUp(sJump);
        bFirePressed = Input.GetButtonDown(sFire);
        bFireHeld = Input.GetButton(sFire);
        bUsePressed = Input.GetButtonDown(sUse);
        bStartPressed = Input.GetButtonDown(sStart);
        bSelectPressed = Input.GetButtonDown(sSelect);
        float tempXInput = Input.GetAxisRaw(sWalkX);
        float tempYInput = Input.GetAxisRaw(sWalkY);
        fWalkXInput = Mathf.Round(tempXInput - tempXInput * -0.3f);
        fWalkYInput = Mathf.Round((tempYInput - tempYInput * -0.3f));
        bLeftPressed = ( tempXInput < -0.95f);
        bRightPressed = ( tempXInput > 0.95f);
        bUpPressed = (Input.GetAxisRaw(sWalkY) > 0.95f); //todo: make this work better
        bDownPressed = (Input.GetAxisRaw(sWalkY) < -0.95f);
    }
}
