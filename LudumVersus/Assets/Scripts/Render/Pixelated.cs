﻿using UnityEngine;
using System.Collections;

public class Pixelated : MonoBehaviour {

    public int pixelSize = 2;
    public Texture targetTexture;
    public Camera currentCamera;

    void Start ()
    {
        currentCamera = GetComponent<Camera>();
        //targetTexture = new RenderTexture(Screen.width / pixelSize, Screen.height / pixelSize, 24, RenderTextureFormat.Default);
        targetTexture = new RenderTexture(480, 270, 24, RenderTextureFormat.Default);
        targetTexture.filterMode = FilterMode.Point;
        targetTexture.anisoLevel = 0;
        targetTexture.mipMapBias = 0;
        currentCamera.targetTexture = targetTexture as RenderTexture;

    }

    // Update is called once per frame
    void OnGUI() {
        Graphics.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), targetTexture);
    }
}
