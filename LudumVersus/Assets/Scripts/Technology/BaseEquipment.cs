﻿using UnityEngine;
using System.Collections;

public class BaseEquipment : MonoBehaviour {

    public int buyValue = 0;

    public Animator animator;

    public SpriteRenderer sprite;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
