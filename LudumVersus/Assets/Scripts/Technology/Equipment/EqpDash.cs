﻿using UnityEngine;
using System.Collections;

public class EqpDash : BaseEquipment
{

    PlayerInputManager currentInput;
    BasePlayer currentPlayer;

    int baseDashTime = 10;
    int currentDashTime = 0;
    int currentBashDelay = 0;
    float dashSpeed = 3f;
    Vector2 velocityBeforeDash;
    bool HasPlayedSound = false;

    // Use this for initialization
    void Start()
    {
        currentInput = GetComponent<PlayerInputManager>();
        currentPlayer = GetComponent<BasePlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentInput.bUsePressed && currentBashDelay < 0)
        {
            if (!HasPlayedSound) {
                SoundManager.PlaySound("Dash");
                HasPlayedSound = true;
            }
            currentDashTime = baseDashTime;
            currentPlayer.currentMovementDelay = baseDashTime;
            currentPlayer.body.AddRelativeForce(currentPlayer.currentDirection * 100f);
            velocityBeforeDash = currentPlayer.body.velocity;
            currentBashDelay = baseDashTime * 3;
        }
        if (currentDashTime > 0)
        {
            currentPlayer.body.velocity = currentPlayer.currentDirection * dashSpeed;
            currentPlayer.body.gravityScale = 0;
            currentDashTime--;
        }
        if (currentDashTime == 0)
        {
            currentPlayer.body.velocity = new Vector2( (velocityBeforeDash.x + currentPlayer.currentDirection.x * dashSpeed) /2, 0f);
            currentDashTime--;
            HasPlayedSound = false;
        }
        if(currentBashDelay >= 0)
        {
            currentBashDelay--;
        }
    }
}
