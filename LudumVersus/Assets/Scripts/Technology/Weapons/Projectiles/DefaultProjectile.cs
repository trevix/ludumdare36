﻿using UnityEngine;
using System.Collections;

public class DefaultProjectile : MonoBehaviour {

    public Vector2 direction;
    public float speed;
    public float lifeSpan; //in seconds
    public int damage;
    public int pushForce;
    public PlayerIdentifier parentId;

    private Rigidbody2D body;
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        Vector2 moveDirection = new Vector2(direction.x, direction.y) * speed;
        body.velocity = moveDirection;
        if (moveDirection != Vector2.zero)
        {
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        StartCoroutine(hitDelay());
    }

    IEnumerator hitDelay ()
    {
        yield return new WaitForSeconds(0.25f);
        parentId = PlayerIdentifier.AI;
    }
	
    public void SetTargetDirection (Vector2 _direction)
    {
        direction = _direction;
    }

    public void SetParentId (PlayerIdentifier _parentId)
    {
        parentId = _parentId;
    }

    void OnTriggerEnter2D (Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 9:
                BasePlayer targetHit = collision.gameObject.GetComponent<BasePlayer>();
                if (targetHit.currentInput.assignedPlayer != parentId)
                {
                    targetHit.health.TakeHit(damage);
                    targetHit.PushOff(gameObject, pushForce);
                    GameObject.Destroy(gameObject);
                }
                break;
            case 8:
                GameObject.Destroy(gameObject);
                break;
            default:
                break;
        }
    }
}
