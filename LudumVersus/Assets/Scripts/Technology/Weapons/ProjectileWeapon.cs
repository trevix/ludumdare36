﻿using UnityEngine;
using System.Collections;

public class ProjectileWeapon : MonoBehaviour {

    PlayerInputManager currentInput;
    BasePlayer currentPlayer;

    public int ammo = 12; //ammo = -1 infinite ammo
    public int fireDelay = 12;
    public int currentDelay = 0;
    // Use this for initialization
    public string SFXTitle = "Shoot";
    public string ProjectileName = "PistolBullet";
    public string spriteName = "";
    private GameObject targetProjectile;
    public bool canAim = false;
    
    void Start()
    {
        currentInput = GetComponent<PlayerInputManager>();
        currentPlayer = GetComponent<BasePlayer>();
        SetTargetProjectile();
    }

    public void ChangeWeaponStatus(string _ProjectileName, string _SFXTitle, string _spriteName, int _ammo = 1, int _fireDelay = 0, bool _canAim = false)
    {
        ProjectileName = _ProjectileName;
        SFXTitle = _SFXTitle;
        spriteName = _spriteName;
        ammo = _ammo;
        fireDelay = _fireDelay;
        canAim = _canAim;
        SetTargetProjectile();
    }

    public void SetTargetProjectile(string newProjectile = null)
    {
        if (newProjectile != null)
            ProjectileName = newProjectile;
        targetProjectile = Resources.Load<GameObject>("Weapons/Projectiles/" + ProjectileName);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentInput.bFirePressed && currentDelay <= 0 && ammo != 0)
        {
            GameObject newProjectile = Instantiate(targetProjectile);
            newProjectile.transform.position = currentPlayer.gameObject.transform.position + new Vector3(currentPlayer.currentDirection.x * 0.1f, currentPlayer.currentDirection.y, 0f);
            DefaultProjectile projectileScript = newProjectile.GetComponent<DefaultProjectile>();

            Vector2 targetDirection = new Vector2(0,0);
            if ( (currentInput.bUpPressed || currentInput.bDownPressed) && canAim) //if can aim, shoots up or down if pressed
            {
                targetDirection.y = currentInput.fWalkYInput*-1f; //input is inverted
                if (currentInput.bLeftPressed || currentInput.bRightPressed) //if left or right is pressed, shoots diagonal
                {
                    targetDirection.x = currentPlayer.currentDirection.x;
                }
            } else //if it's not pressed up or down, shoot in the looking directiong
            {
                targetDirection.x = currentPlayer.currentDirection.x;
            }

            projectileScript.SetTargetDirection(targetDirection.normalized);
            projectileScript.SetParentId(currentInput.assignedPlayer);

            ammo--;
            currentDelay = fireDelay;
            SoundManager.PlaySound(SFXTitle);
            if (ammo == 0)
            {
                currentPlayer.DropWeapon();
            }
        }
        if (currentDelay > 0)
        {
            currentDelay--;
        }


    }
}
