﻿using UnityEngine;
using System.Collections;
using System;

public class ItemPickup : MonoBehaviour {

    public string targetItem = null;
    [HideInInspector]
    public SpriteRenderer sprite;
    [HideInInspector]
    public Collider2D collider;

    public void Start () {
        sprite = GetComponent<SpriteRenderer>();
        collider = GetComponent<Collider2D>();
    }

    public void SetTargetItem (string _target) {
        targetItem = _target;
    }

    public void disableMe () {
        StartCoroutine(HideMe());
    }

    IEnumerator HideMe ( ) {
        sprite.enabled = false;
        collider.enabled = false;
        yield return new WaitForSeconds(15);
        sprite.enabled = true;
        collider.enabled = true; 
    }

}
