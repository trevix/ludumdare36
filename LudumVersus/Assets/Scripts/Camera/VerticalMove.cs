﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class VerticalMove : MonoBehaviour {

    public float moveSpeed = 1.0f;

    private bool hasStarted = false;
	// Use this for initialization
	void Start () {
        StartCoroutine(enableMovement());
    }

    IEnumerator enableMovement() {
        yield return new WaitForSeconds(4f);
        hasStarted = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (hasStarted) {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + moveSpeed, -1f);
        }
        

    }
}
