﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class HorizontalFollow : MonoBehaviour {

    BasePlayer[] players;
    Vector2 cameraPosition;

	// Use this for initialization
	void Start () {
        players = GameObject.FindObjectsOfType<BasePlayer>();
        cameraPosition = gameObject.transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        //sort per position
        Array.Sort(players, delegate (BasePlayer playerA, BasePlayer playerB) {
            return playerA.transform.position.x.CompareTo(playerB.transform.position.x);
        });
        Array.Reverse(players);
        if (players[1].isAlive)
        {
            float xPosition = (players[0].transform.position.x * 2 + players[1].transform.position.x) / 3f;
            gameObject.transform.position = new Vector3(xPosition, gameObject.transform.position.y, -1f);
        } else
        {
            gameObject.transform.position = new Vector3( (players[0].transform.position.x+ gameObject.transform.position.x*9)/10, gameObject.transform.position.y, -1f);
        }
        
	}
}
