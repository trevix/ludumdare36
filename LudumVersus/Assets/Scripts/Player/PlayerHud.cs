﻿using UnityEngine;
using System.Collections;

public class PlayerHud : MonoBehaviour {


    public PlayerIdentifier currentPlayerID;
    // Use this for initialization
    GameObject HUDObject;
    GameObject LifeProgress;
	
    public void SetAttachedPlayer (PlayerIdentifier _currentPlayerID) {
        currentPlayerID = _currentPlayerID;
        SetAttachedObjects();
    }
	
    void SetAttachedObjects () {
        GameObject masterHud = GameObject.Find("HUD");
        HUDObject = GameObject.Find("HUD"+currentPlayerID);
        LifeProgress = GameObject.Find("life-" + currentPlayerID);
    }

    public void HideHud (PlayerIdentifier _id) {
        SetAttachedPlayer(_id);
        HUDObject.gameObject.SetActive(false);
    }
	
    //life-P1
    public void SetBarSize (float _progress) {
        //
    }
}
