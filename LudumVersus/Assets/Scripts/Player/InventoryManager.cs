﻿using UnityEngine;
using System.Collections;

public class InventoryManager : MonoBehaviour {

    public BasePlayer currentPlayer;

    public ProjectileWeapon currentProjectileWeapon;
    //WEAPON LIST
    public WpnHand wpnHand = null;

    //EQUIPMENT LIST
    public EqpEmpty eqpEmpty = null;
    public EqpDash eqpDash = null;


    // Use this for initialization
    void Start ( ) {
        currentPlayer = GetComponent<BasePlayer>();

        eqpEmpty = gameObject.AddComponent<EqpEmpty>();
        eqpDash = gameObject.AddComponent<EqpDash>();

        DisableAllEquipment();
        DisableAllWeapons();
        //EnableEquipment(eqpDash);
        switch (GameManager.currentGameMode) { //this changes the player equipment based on game mode
            default:
                EnableWeapon(wpnHand);
                break;
        }
    }

    public void DisableAllWeapons ( ) {
        ProjectileWeapon playerWeapon = currentPlayer.gameObject.GetComponent<ProjectileWeapon>();
        //playerWeapon.ammo = 0;
    }

    void EnableWeapon ( MonoBehaviour _weaponType ) {
        DisableAllWeapons();
    }

    void DisableAllEquipment ( ) {
        eqpEmpty.enabled = false;
        eqpDash.enabled = false;
    }

    void EnableEquipment ( MonoBehaviour _equipType ) {
        DisableAllEquipment();
        MonoBehaviour temp = GetComponent(_equipType.GetType()) as MonoBehaviour;
        temp.enabled = true;
    }

    public void DropWeapon ( ) {

    }

    public void GetItem ( string _targetItem ) {
        switch (_targetItem) {
            case "EqpEmpty":
                EnableEquipment(eqpEmpty);
                break;
            case "EqpDash":
                EnableEquipment(eqpDash);
                break;
        }
    }
}
