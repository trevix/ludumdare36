﻿using UnityEngine;
using System.Collections;

public class BasePlayer : MonoBehaviour {

    public SpriteRenderer sprite;
    public Rigidbody2D body;
    public Animator animator;
    public PlayerHealth health;
    public PlayerIdentifier playerId;
    public PlayerHud playerHud;
    public InventoryManager myInventoryManager;
    public SpriteOutline outlineColor;

    int baseHitDelay = 30;
    public int currentHitDelay = 0;
    public int baseJumpDelay = 10;
    public int currentJumpDelay = 0;

    private float walkSpeed = 1.25f;
    private float jumpForce = 45f;
    private float wallJumpForce = 40f;
    private int floatTime = 10;
    private float gravityValue = 0.8f;
    private float gravityWallSlideModifier = 0.6f;
    private int JumpDamage = 3;
    private float slideSpeed = -0.08f;
    private float maxFallSpeed = -3f;

    [HideInInspector]
    private bool isOnTopOfPlayer = false;
    private bool isOnGround = false;
    private bool isOnRightWall = false;
    private bool isOnLeftWall = false;
    private bool isOnlyFootOnWall = false;
    private bool isGrabbingALedge = false;
    public CircleCollider2D groundCheck;
    public CircleCollider2D rightCheck;
    public CircleCollider2D leftCheck;
    public CircleCollider2D footCheck;
    public CircleCollider2D bodyCollider;
    private float onAirDrag = 1f;
    private float stoppedDrag = 10f;
    private float movingDrag = 1f;


    public float onGroundGravity = 0.00f; //this influences the speed on ramps
    private float airControl = 0.5f;
    //longer jumps
    
    private int currentFloatValue = 0;

    //walljump delays
    private int baseWallJumpDelay = 12;
    public int currentMovementDelay = 0;

    //stun delay
    private int baseStunDelay = 30;
    public bool isStunned = false;
    public bool isAlive = true;

    //direction
    public Vector2 currentDirection = Vector2.right;
    public PlayerInputManager currentInput;

    public bool isAttacking = false;
    public int attackDelay = 0;
    
    void Start ( ) {
        currentInput = GetComponent<PlayerInputManager>();
        playerId = currentInput.assignedPlayer;
        outlineColor = GetComponent<SpriteOutline>();
        //creeateEquipments and weapons
        myInventoryManager = gameObject.AddComponent<InventoryManager>();

        playerHud = gameObject.AddComponent<PlayerHud>();
        playerHud.SetAttachedPlayer(playerId);

        currentDirection = Vector2.right;
        animator = animator;
    }

    // Use this for initialization
    void Awake ( ) {
        health = gameObject.AddComponent<PlayerHealth>();
        animator = GetComponent<Animator>();
        bodyCollider = this.GetComponent<CircleCollider2D>();
        body = this.GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        CreateColliders();
    }

    void CreateColliders ( ) {
        groundCheck = gameObject.AddComponent<CircleCollider2D>();
        groundCheck.radius = bodyCollider.radius * 0.8f;
        groundCheck.offset = new Vector2(0, bodyCollider.offset.y + bodyCollider.radius * -0.4f);
        groundCheck.isTrigger = true;
        
        rightCheck = gameObject.AddComponent<CircleCollider2D>();
        rightCheck.radius = bodyCollider.radius * 0.8f;
        rightCheck.offset = new Vector2(bodyCollider.radius * 0.3f, bodyCollider.offset.y + 0.01f);
        rightCheck.isTrigger = true;

        leftCheck = gameObject.AddComponent<CircleCollider2D>();
        leftCheck.radius = bodyCollider.radius * 0.8f;
        leftCheck.offset = new Vector2(bodyCollider.radius * -0.3f, bodyCollider.offset.y + 0.01f);
        leftCheck.isTrigger = true;
        
        footCheck = gameObject.AddComponent<CircleCollider2D>();
        footCheck.radius = bodyCollider.radius * 1.05f;
        footCheck.offset = new Vector2(0, bodyCollider.offset.y + bodyCollider.radius *-0.1f);
        footCheck.isTrigger = true;
        
    }

    void Update ( ) {
        var jumpPressed = currentInput.bJumpPressed;
        if (jumpPressed) {
            if (isOnGround) {
                //NORMAL JUMP!
                Jump();
                if(isOnLeftWall || isOnRightWall) { //this prevents the player from being stuck on the wall
                    currentMovementDelay = baseWallJumpDelay;
                }
            } else { //WALLJUMP!
                if (isOnLeftWall) {
                    WallJump(Vector2.right);
                }
                if (isOnRightWall) {
                    WallJump(Vector2.left);
                }
                /*if (isGrabbingALedge) { //"walljump" straigh up
                    Jump();
                    currentMovementDelay = baseWallJumpDelay;
                }*/
            }
        }
    }

    void WallJump (Vector2 _direction) {
        if(currentJumpDelay >= 0) {
            return;
        }
        Jump();
        body.AddRelativeForce(_direction * wallJumpForce);
        currentMovementDelay = baseWallJumpDelay;
        SoundManager.PlaySound("Jump");
    }

    void Jump () {
        if(currentJumpDelay >= 0) {
            return;
        }
        currentJumpDelay = baseJumpDelay;
        body.velocity = new Vector2(body.velocity.x, 0);
        body.AddRelativeForce(Vector2.up * jumpForce);
        currentFloatValue = floatTime;
        SoundManager.PlaySound("Jump");
    }

    // Update is called once per frame
    void FixedUpdate ( ) {
        calcPhysics();
    }


    void checkDelays () {
        currentJumpDelay--;
        currentMovementDelay--;
        currentHitDelay--;
    }

    void calcPhysics ( ) {
        isOnGround = groundCheck.IsTouchingLayers(1 << LayerMask.NameToLayer("Ground"));
        isOnTopOfPlayer = groundCheck.IsTouchingLayers(1 << LayerMask.NameToLayer("Player"));
        isOnLeftWall = leftCheck.IsTouchingLayers(1 << LayerMask.NameToLayer("Ground"));
        isOnRightWall = rightCheck.IsTouchingLayers(1 << LayerMask.NameToLayer("Ground"));
        isOnlyFootOnWall = footCheck.IsTouchingLayers(1 << LayerMask.NameToLayer("Ground")) && !isOnLeftWall && !isOnRightWall;
        isGrabbingALedge = (isOnlyFootOnWall && !isOnGround && !( Mathf.Abs(body.velocity.magnitude) > 0f ) );
        checkDelays();  
        if (currentHitDelay > 0) {
            sprite.enabled = !sprite.enabled;
        } else {
            sprite.enabled = true;
        }
        
        if (isOnTopOfPlayer) { //being over another player counts as ground.
            isOnGround = true;
        }

        var horizontal = currentInput.fWalkXInput;
        var jumpPressed = currentInput.bJumpPressed;
        var jumpHeld = currentInput.bJumpDown;
        var jumpreleased = currentInput.bJumpReleased;

        if (!isAlive) {
            horizontal = 0f;
            jumpPressed = false;
            jumpHeld = false;
            jumpreleased = false;
        }

        var realWalkSpeed = walkSpeed;

        if (isOnGround) {
            body.gravityScale = onGroundGravity; //when walking, to ignore ramp gravity force
        } else {
            //is on air
            body.drag = onAirDrag; //do not stop when on a jump
            if (body.velocity.y < maxFallSpeed) //make player not fall too fast
            {
                body.velocity = new Vector2(body.velocity.x, maxFallSpeed);
                body.gravityScale = 0f; //we all must fall. 
            } else
            {
                body.gravityScale = gravityValue; //we all must fall.
            }
            
            if (isOnLeftWall || isOnRightWall) {
                body.gravityScale *= gravityWallSlideModifier;
            }
            realWalkSpeed *= airControl;
        }
        
        if (currentInput.bLeftPressed || currentInput.bRightPressed ) { //forces horizontal movement
            if (currentMovementDelay <= 0) {
                if ( currentInput.bLeftPressed && !isOnLeftWall) {
                    float targetXVelocity = ( realWalkSpeed * horizontal + body.velocity.x*99 ) / 100;
                    if(targetXVelocity < body.velocity.x) //forces left only if velocity is smaller
                    {
                        body.velocity = new Vector2(realWalkSpeed * horizontal, body.velocity.y);
                    }
                } else {
                    if (currentInput.bRightPressed && !isOnRightWall) {
                        float targetXVelocity = ( realWalkSpeed * horizontal + body.velocity.x*99 ) / 100;
                        if (targetXVelocity > body.velocity.x) //forces left only if velocity is smaller
                        {
                            body.velocity = new Vector2(realWalkSpeed * horizontal, body.velocity.y);
                        }
                    }
                }
            }
            if (isOnGround) {
                if (currentMovementDelay <= 0) {
                    body.drag = movingDrag;
                }
            }
            if ( (isOnLeftWall || isOnRightWall) && body.velocity.y < 0) {
                body.velocity = new Vector2(body.velocity.x, slideSpeed);
            }
        } else {
            if (isOnGround) {
                if(currentMovementDelay <= 0) {
                    body.drag = stoppedDrag;
                }
            }
        }
        if(currentMovementDelay == 0) {
            isStunned = false;
        }

        if (jumpHeld && currentFloatValue > 0) {
            body.AddRelativeForce(Vector2.up * jumpForce * ( currentFloatValue / floatTime ));
            body.gravityScale = ( currentFloatValue / floatTime ) * gravityValue;
            currentFloatValue--;
        }
        if (jumpreleased) {
            currentFloatValue = 0;
        }
        if(isAlive)
            CheckDirection();
        if (!isAttacking) {
            checkAnimationState();
        }

    }

    public void die () {
        isAlive = false;
        bodyCollider.radius = bodyCollider.radius * 0.5f;
        bodyCollider.offset = new Vector2(0, -0.11f);
        body.mass = body.mass * 0.5f;
        groundCheck.enabled = false;
        leftCheck.enabled = false;
        rightCheck.enabled = false;
        myInventoryManager.DisableAllWeapons();
        //this is where we should treat the end of the level
        GameManager.numberOfPlayersAlive--;
        GameManager.KillPlayer(currentInput.assignedPlayer);
        currentInput.UpdateAssignedController(PlayerIdentifier.AI);
    }

    public void knockDown (Vector2 _currentDirection, float _force) {
        body.AddForce(_currentDirection * _force);
    }

    public void DamageStun () {
        currentHitDelay = baseHitDelay;
    }

    void checkAnimationState ( ) {
        if (!isAlive) {
            animator.Play("Death");
            return;
        }
        if (isOnGround) {
            if (Mathf.Abs(body.velocity.x) != 0f && currentInput.fWalkXInput != 0f) {
                animator.Play("Walk");
            } else {
                animator.Play("Idle");
            }
        } else {
            if (!isOnGround) {
                if (body.velocity.y <= 0.1f) {
                    if (!isOnLeftWall && !isOnRightWall) {
                        animator.Play("Fall");
                    } else {
                        animator.Play("Wallslide");
                    }

                } else {
                    if (body.velocity.y > -0.1f) {
                        animator.Play("Jump");
                    }
                }
            }
        }

    }

    void BounceOffPlayer (BasePlayer _gameObject, GameObject _thisObj) {
        Vector2 direction = _gameObject.transform.position - _thisObj.transform.position;
        _gameObject.body.velocity = new Vector2(direction.x*5f, direction.y*5f );
        _gameObject.currentMovementDelay = _gameObject.baseStunDelay/2;
        _gameObject.body.drag = 0;
    }

    public void PushOff (GameObject _thisObj, float force) {
        Vector2 direction = this.transform.position - _thisObj.transform.position;
        this.body.velocity = new Vector2(direction.x * force, direction.y * force + force/3f);
        this.currentMovementDelay = baseStunDelay;
        this.body.drag = 0;
    }

    void OnCollisionEnter2D ( Collision2D col ) {
        if (!isAlive) {
            return;
        }
        
        if (col.gameObject.layer == 9) {
            isOnTopOfPlayer = groundCheck.IsTouchingLayers(1 << LayerMask.NameToLayer("Player"));
            var objectPlayer = col.gameObject.GetComponent<BasePlayer>();
            if (isOnTopOfPlayer && (Mathf.Round(col.gameObject.transform.position.y*100) < Mathf.Round(gameObject.transform.position.y*100)) ) {
                Jump();
                objectPlayer.health.TakeHit(JumpDamage);
            } else {
                if (objectPlayer.currentMovementDelay <= 0) {
                    objectPlayer.BounceOffPlayer(objectPlayer, gameObject);
                }
                
            }
        }

        if (col.gameObject.layer == 11) {
            health.TakeHit(100);
        }
        if (col.gameObject.layer == 12)
        {
            GameManager.AddScoreToPlayer(currentInput.assignedPlayer);
            Destroy(col.gameObject);
            LevelManager.GetInstance().CallNextLevel();
        }
    }

    void OnTriggerEnter2D ( Collider2D col ) {
        if (col.gameObject.layer == 10) {
            SoundManager.PlaySound("Pickup");
            ItemPickup item = col.gameObject.GetComponent<ItemPickup>();
            GetItem(item.targetItem);
            item.disableMe();
        }
    }

    void CheckDirection ( ) {
        if (currentInput.bRightPressed) {
            currentDirection = Vector2.right;
            sprite.flipX = false;
        }
        if (currentInput.bLeftPressed) {
            currentDirection = Vector2.left;
            sprite.flipX = true;
        }
    }

    //interface for calling the inventory
    public void DropWeapon ( ) {
        myInventoryManager.DropWeapon();
    }

    public void GetItem (string _targetItem) {
        myInventoryManager.GetItem(_targetItem);
    }
}


