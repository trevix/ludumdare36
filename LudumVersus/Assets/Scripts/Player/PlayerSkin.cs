﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerSkin : MonoBehaviour {

    public string SpriteSheetName = "";
    Sprite[] LoadedSprite;
    SpriteRenderer PlayerRenderer;
	// Use this for initialization
	void Start () {
        //SpriteSheetName = "black";
        LoadedSprite = Resources.LoadAll<Sprite>("Animations/Player/"+ SpriteSheetName);
        PlayerRenderer = GetComponentInChildren<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (SpriteSheetName == "")
            return;
        string spriteName = PlayerRenderer.sprite.name;
        Sprite newSprite = Array.Find(LoadedSprite, item => item.name == spriteName);
        if (newSprite)
            PlayerRenderer.sprite = newSprite;
    }
}
