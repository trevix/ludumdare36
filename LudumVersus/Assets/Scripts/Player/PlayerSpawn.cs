﻿using UnityEngine;
using System.Collections;

public class PlayerSpawn : MonoBehaviour {

    GameObject childPlayer = null;
    BasePlayer basePlayer = null;
    PlayerIdentifier playerId = PlayerIdentifier.AI;

	// Use this for initialization
	void Awake () {
        //TODO: 
        childPlayer = Instantiate(Resources.Load("Player"), gameObject.transform.position, Quaternion.identity) as GameObject;
        basePlayer = childPlayer.GetComponent<BasePlayer>();
    }

    public void UpdateAssignedController (PlayerIdentifier _target)
    {
        playerId = _target;
        childPlayer.name = "player_" + _target;
        PlayerInputManager assignedControllerManager = childPlayer.GetComponent<PlayerInputManager>();
        assignedControllerManager.UpdateAssignedController(_target);
        switch (_target)
        {
            case PlayerIdentifier.P1:
                basePlayer.outlineColor.color = Color.red;
                break;
            case PlayerIdentifier.P2:
                basePlayer.outlineColor.color = Color.green;
                break;
            case PlayerIdentifier.P3:
                basePlayer.outlineColor.color = Color.blue;
                break;
            case PlayerIdentifier.P4:
                basePlayer.outlineColor.color = Color.yellow;
                break;
            case PlayerIdentifier.AI:
                basePlayer.outlineColor.color = Color.black;
                break;
        }
    }

    void OnDrawGizmos () {
        Gizmos.DrawIcon(transform.position, "spawn.png", true);
    }

    public void DisablePlayer (bool _disable) {
        childPlayer.SetActive(_disable);
        if (!_disable) {
            GameManager.numberOfPlayersAlive--;
        }
    }
}
