﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

    public int health = 6;
    public int maxHealth = 6;

    public BasePlayer currentPlayer;

	// Use this for initialization
	void Start () {
        currentPlayer = GetComponent<BasePlayer>();
    }
	
    public void TakeHit (int _amount) {
        if(currentPlayer.currentHitDelay > 0) {
            return;
        }
        if(health > 0) {
            health -= _amount;
            currentPlayer.DamageStun();
            SoundManager.PlaySound("Hit");
            if (health <= 0) {
                currentPlayer.die();
            }
        }
        currentPlayer.playerHud.SetBarSize(health / maxHealth);
    }
	// Update is called once per frame
	void Update () {
	
	}
}
