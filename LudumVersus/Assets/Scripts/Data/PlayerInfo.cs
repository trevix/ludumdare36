﻿using UnityEngine;
using System.Collections;

public class PlayerInfo {

    public PlayerIdentifier playerId;
    public string playerName;
    public bool isAlive = false;
    public int techValue = 0;
    public int winCount = 0;
    public int killCount = 0;


}
