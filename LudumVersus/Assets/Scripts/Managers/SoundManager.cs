﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

public class SoundManager : MonoBehaviour
{

    public static SoundManager instance = null;

    public AudioClip BGMFight = null;
    public AudioSource BGMFSource = null;
    public AudioClip BGMMenu = null;
    public AudioSource BGMMSource = null;
    public AudioClip SFXConfirm = null;
    public AudioClip SFXCancel = null;
    public List<AudioClip> SFXAttackList = new List<AudioClip>();
    public List<AudioClip> SFXShootList = new List<AudioClip>();
    public AudioClip SFXThrow = null;
    public List<AudioClip> SFXGetHitList = new List<AudioClip>();
    public List<AudioClip> SFXDeathList = new List<AudioClip>();
    public AudioClip SFXJump = null;
    public AudioClip SFXPickUp = null;
    public AudioClip SFXDash = null;


    public bool isBGMEnabled = false;
    public bool isSFXEnabled = false;

    public static SoundManager GetInstance()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<SoundManager>();
            if (instance == null)
            {
                GameObject newInstance = new GameObject("Sound Manager");
                newInstance.AddComponent<SoundManager>();
                Instantiate(newInstance);
                instance = FindObjectOfType<SoundManager>();
            }
        }
        return instance;
    }


    void Start()
    {
        SoundManager.GetInstance();
        SoundManager.instance.BGMFSource = instance.gameObject.AddComponent<AudioSource>();
        SoundManager.instance.BGMFSource.clip = instance.BGMFight;
        SoundManager.instance.BGMFSource.loop = true;
        SoundManager.instance.BGMMSource = instance.gameObject.AddComponent<AudioSource>();
        SoundManager.instance.BGMMSource.clip = instance.BGMMenu;
        SoundManager.instance.BGMMSource.loop = true;
        PlayMainMenuBGM();
        DontDestroyOnLoad(transform.gameObject);
    }
    

    public static void PlaySound(string _SFXName)
    {
        if (!SoundManager.GetInstance().isSFXEnabled) {
            return;
        }
        AudioClip targetClip = null;

        switch (_SFXName)
        {
            case "Confirm":
                targetClip = instance.SFXConfirm;
                break;
            case "Cancel":
                targetClip = instance.SFXCancel;
                break;
            case "Throw":
                targetClip = instance.SFXThrow;
                break;
            case "Jump":
                targetClip = instance.SFXJump;
                break;
            case "Pickup":
                targetClip = instance.SFXPickUp;
                break;
            case "Dash":
                targetClip = instance.SFXDash;
                break;
            case "Attack":
                targetClip = instance.SFXAttackList[UnityEngine.Random.Range(0, instance.SFXAttackList.Count)];
                break;
            case "Shoot":
                targetClip = instance.SFXShootList[UnityEngine.Random.Range(0, instance.SFXShootList.Count)];
                break;
            case "Hit":
                targetClip = instance.SFXGetHitList[UnityEngine.Random.Range(0, instance.SFXGetHitList.Count)];
                break;
            case "Death":
                targetClip = instance.SFXDeathList[UnityEngine.Random.Range(0, instance.SFXDeathList.Count)];
                break;
                
        }

        GameObject tempObj = new GameObject();
        AudioSource temp = tempObj.AddComponent<AudioSource>();
        Instantiate(tempObj);
        DontDestroyOnLoad(tempObj);
        temp.clip = targetClip;
        temp.Play();
        Destroy(tempObj, targetClip.length);

    }

    public static void PlayMainMenuBGM ()
    {
        if (!SoundManager.GetInstance().isBGMEnabled) {
            return;
        }
        StopBGM();
        instance.BGMMSource.Play();
    }

    public static void PlayFightBGM ()
    {
        if (!SoundManager.GetInstance().isBGMEnabled) {
            return;
        }
        StopBGM();
        instance.BGMFSource.Play();

    }

    public static void StopBGM () {
        instance.BGMMSource.Stop();
        instance.BGMFSource.Stop();
    }

    void Update()
    {

    }

}