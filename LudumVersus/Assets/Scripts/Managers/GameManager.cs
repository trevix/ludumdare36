﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

    public static List<PlayerInfo> playerList = new List<PlayerInfo>();

    public static bool p1Active = false;
    public static bool p2Active = false;
    public static bool p3Active = false;
    public static bool p4Active = false;

    public static bool p1Alive = false;
    public static bool p2Alive = false;
    public static bool p3Alive = false;
    public static bool p4Alive = false;

    public static int numberOfPlayersAlive = 4;

    public static int currentLevelId = 0;
    public static string currentGameMode = "Death Match";

    public static List<string> levelList = new List<string>();

    public static GameManager GetInstance ( ) {
        if (instance == null) {
            instance = FindObjectOfType<GameManager>();
            if (instance == null) {
                GameObject newInstance = new GameObject("Game Manager");
                newInstance.AddComponent<GameManager>();
                instance = FindObjectOfType<GameManager>();
                
            }
        }
        return instance;
    }


    void Start ( ) {
        DontDestroyOnLoad(transform.gameObject);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Application.targetFrameRate = 60;
        CreatePlayerData(4);
    }

    public static void CreateDeathMatchLevelList() {
        levelList.Clear();
        levelList.Add("Level 1");
    }
    public static void CreateBeachBallLevelList ( ) {
        levelList.Clear();
        levelList.Add("Level 1");
    }
    public static void CreateRaceLevelList()
    {
        levelList.Clear();
        levelList.Add("rc-level1");
    }

    public static void CreateTowerClimbLevelList() {
        levelList.Clear();
        levelList.Add("tc-level1");
    }

    public static void NextLevel () {
        if(levelList.Count == 0) {
            GameManager.CreateDeathMatchLevelList();
        }
        ResetPlayerStatus();
        GameManager.numberOfPlayersAlive = 4;
        
        if(currentLevelId< levelList.Count) {
            SceneManager.LoadScene( levelList[currentLevelId]);
            currentLevelId++;
        } else {
            currentLevelId = 0;
            SceneManager.LoadScene("Results");
        }
        
    }

    public static void ChangeGameMode (string _selectedMode)
    {
        switch (_selectedMode)
        {
            case "Classic DeathMatch":
                CreateDeathMatchLevelList();
                break;
            case "BeachBall Fight":
                CreateBeachBallLevelList();
                break;
            case "Race":
                CreateRaceLevelList();
                break;
            case "Towerclimb":
                CreateTowerClimbLevelList();
                break;
            default:
                CreateDeathMatchLevelList();
                break;
        }
        currentGameMode = _selectedMode;
        GameManager.CreatePlayerData(4);
    }

    public static void CreatePlayerData ( int _numPlayers) {
        playerList.Clear();
        while (_numPlayers > 0) {
            PlayerInfo tempPlayer = new PlayerInfo();
            switch (_numPlayers) {
                case 4:
                    tempPlayer.playerId = PlayerIdentifier.P1;
                    break;
                case 3:
                    tempPlayer.playerId = PlayerIdentifier.P2;
                    break;
                case 2:
                    tempPlayer.playerId = PlayerIdentifier.P3;
                    break;
                case 1:
                    tempPlayer.playerId = PlayerIdentifier.P4;
                    break;
            }
            tempPlayer.playerName = "player_" + tempPlayer.playerId;
            playerList.Add(tempPlayer);
            _numPlayers--;
        }
    }

    public static void ResetPlayerStatus () {
        foreach (PlayerInfo player in playerList) {
            player.isAlive = true;
        }
    }

    public static void KillPlayer (PlayerIdentifier _playerId) {
        foreach (PlayerInfo player in playerList) {
            if (player.playerId == _playerId) {
                player.isAlive = false;
            }
        }
    }

    public static void VerifyAlivePlayers () {
        foreach (PlayerInfo player in playerList) {
            if (player.isAlive) {
                player.killCount++;
            }
        }
    }

    public static void AddScoreToPlayer(PlayerIdentifier _id)
    {
        foreach (PlayerInfo player in playerList)
        {
            if (player.playerId == _id)
            {
                player.killCount++;
            }
        }
    }

    public static string GetWinningPlayer ( ) {
        Debug.Log(playerList);
        playerList.Sort(( x, y ) => x.killCount.CompareTo(y.killCount));
        Debug.Log(playerList);
        return playerList[0].playerId + "";
    }

    void Update ( ) {

    }

}