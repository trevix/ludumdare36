﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerManager {

    public static PlayerManager instance = null;

    public BasePlayer[] playerList;
	// Use this for initialization
	void Start () {
        updatePlayerList();
	}

    public static PlayerManager GetInstance ()
    {
        if(instance == null)
        {
            instance = new PlayerManager();
            instance.updatePlayerList();
        }
        return instance;
    }
	
    public void updatePlayerList ()
    {
        playerList = GameObject.FindObjectsOfType<BasePlayer>();
    }

    public BasePlayer[] GetPlayerList ()
    {
        return playerList;
    }
}
