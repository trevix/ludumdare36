﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    PlayerInputManager p1Control;
    PlayerInputManager p2Control;
    PlayerInputManager p3Control;
    PlayerInputManager p4Control;

    TextMesh selectedText;
    TextMesh selectedUpperText;
    TextMesh selectedLowerText;

    bool isOnInputDelay = false;

    List<string> availableOptions = new List<string>();
    int currentSelectedOption = 0;
    // Use this for initialization
    void Start()
    {
        p1Control = gameObject.AddComponent<PlayerInputManager>();
        p1Control.assignedPlayer = PlayerIdentifier.P1;
        p2Control = gameObject.AddComponent<PlayerInputManager>();
        p2Control.assignedPlayer = PlayerIdentifier.P2;
        p3Control = gameObject.AddComponent<PlayerInputManager>();
        p3Control.assignedPlayer = PlayerIdentifier.P3;
        p4Control = gameObject.AddComponent<PlayerInputManager>();
        p4Control.assignedPlayer = PlayerIdentifier.P4;

        selectedText = GameObject.Find("SelectedText").GetComponent<TextMesh>();
        selectedUpperText = GameObject.Find("SelectedTopText").GetComponent<TextMesh>();
        selectedLowerText = GameObject.Find("SelectedLowText").GetComponent<TextMesh>();
        availableOptions.Add("Classic DeathMatch");
        availableOptions.Add("Race");
        //availableOptions.Add("BeachBall Fight");
        availableOptions.Add("Towerclimb");
        availableOptions.Add("Options");
        availableOptions.Add("Quit");

        currentSelectedOption = 0;
        updateSelectedOption(currentSelectedOption);
    }



    // Update is called once per frame
    void Update()
    {
        if (isOnInputDelay)
            return;
        if (p3Control.bDownPressed )
        {
            currentSelectedOption--;
            if(currentSelectedOption < 0)
            {
                currentSelectedOption = availableOptions.Count-1;
            }
            updateSelectedOption(currentSelectedOption);
            StartCoroutine(WaitForNextInput());
        }

        if (p3Control.bUpPressed)
        {
            currentSelectedOption++;
            if (currentSelectedOption == availableOptions.Count)
            {
                currentSelectedOption = 0;
            }
            updateSelectedOption(currentSelectedOption);
            StartCoroutine(WaitForNextInput());
        }

        if (p1Control.bStartPressed || p2Control.bStartPressed || p3Control.bStartPressed || p4Control.bStartPressed ||
            p1Control.bJumpPressed || p2Control.bJumpPressed || p3Control.bJumpPressed || p4Control.bJumpPressed)
        {
            SelectOption(availableOptions[currentSelectedOption]);
            
        }

    }

    IEnumerator WaitForNextInput()
    {
        isOnInputDelay = true;
        yield return new WaitForSeconds(0.1f);
        isOnInputDelay = false;
    }

    void SelectOption (string option) {
        Debug.Log("options!");
        switch (option) {
            case "Quit":
                Application.Quit();
                break;
            case "Options":
                SceneManager.LoadScene("OptionsScreen");
                break;
            default:
                GameManager.ChangeGameMode(option);
                SceneManager.LoadScene("PlayerSelect");
                break;
        }
        
    }


    void updateSelectedOption (int _id)
    {
        SoundManager.PlaySound("Cancel");
        selectedText.text = availableOptions[_id];
        if (_id != 0) {
            selectedUpperText.text = availableOptions[_id - 1];
        } else {
            selectedUpperText.text = availableOptions[ availableOptions.Count-1];
        }
        if (_id +1 < availableOptions.Count) {
            selectedLowerText.text = availableOptions[_id + 1];
        } else {
            selectedLowerText.text = availableOptions[0];
        }
    }
}
