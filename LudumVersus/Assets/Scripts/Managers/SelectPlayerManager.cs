﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SelectPlayerManager : MonoBehaviour {

    PlayerInputManager p1Control;
    PlayerInputManager p2Control;
    PlayerInputManager p3Control;
    PlayerInputManager p4Control;

    GameObject p1Label;
    GameObject p2Label;
    GameObject p3Label;
    GameObject p4Label;

    // Use this for initialization
    void Start () {
        p1Control = gameObject.AddComponent<PlayerInputManager>();
        p1Control.assignedPlayer = PlayerIdentifier.P1;
        p2Control = gameObject.AddComponent<PlayerInputManager>();
        p2Control.assignedPlayer = PlayerIdentifier.P2;
        p3Control = gameObject.AddComponent<PlayerInputManager>();
        p3Control.assignedPlayer = PlayerIdentifier.P3;
        p4Control = gameObject.AddComponent<PlayerInputManager>();
        p4Control.assignedPlayer = PlayerIdentifier.P4;

        p1Label = GameObject.Find("P1Selected");
        p2Label = GameObject.Find("P2Selected");
        p3Label = GameObject.Find("P3Selected");
        p4Label = GameObject.Find("P4Selected");
    }
	
	// Update is called once per frame
	void Update () {
        if (p1Control.bJumpPressed || p1Control.bStartPressed) {
            GameManager.p1Active = true;
            SoundManager.PlaySound("Confirm");
        }
        if (p1Control.bUsePressed) {
            if (!GameManager.p1Active) {
                ReturnToMainMenu();
            }
            GameManager.p1Active = false;
            SoundManager.PlaySound("Cancel");
        }
        if (p2Control.bJumpPressed || p2Control.bStartPressed) {
            GameManager.p2Active = true;
            SoundManager.PlaySound("Confirm");
        }
        if (p2Control.bUsePressed) {
            if (!GameManager.p2Active) {
                ReturnToMainMenu();
            }
            GameManager.p2Active = false;
            SoundManager.PlaySound("Cancel");
        }
        if (p3Control.bJumpPressed || p3Control.bStartPressed) {
            GameManager.p3Active = true;
            SoundManager.PlaySound("Confirm");
        }
        if (p3Control.bUsePressed) {
            if (!GameManager.p3Active) {
                ReturnToMainMenu();
            }
            GameManager.p3Active = false;
            SoundManager.PlaySound("Cancel");
        }
        if (p4Control.bJumpPressed || p4Control.bStartPressed) {
            GameManager.p4Active = true;
            SoundManager.PlaySound("Confirm");
        }
        if (p4Control.bUsePressed) {
            if (!GameManager.p4Active) {
                ReturnToMainMenu();
            }
            GameManager.p4Active = false;
            SoundManager.PlaySound("Cancel");
        }


        p1Label.SetActive(GameManager.p1Active);
        p2Label.SetActive(GameManager.p2Active);
        p3Label.SetActive(GameManager.p3Active);
        p4Label.SetActive(GameManager.p4Active);


        if (p1Control.bStartPressed || p2Control.bStartPressed || p3Control.bStartPressed || p4Control.bStartPressed) {
            GameManager.currentLevelId = 0;
            SoundManager.StopBGM();
            GameManager.NextLevel();
        }

    }

    void ReturnToMainMenu () {
        SceneManager.LoadScene("MainMenu");
    }
}
