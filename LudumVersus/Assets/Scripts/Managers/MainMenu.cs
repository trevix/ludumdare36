﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    PlayerInputManager p1Control;
    PlayerInputManager p2Control;
    PlayerInputManager p3Control;
    PlayerInputManager p4Control;

	// Use this for initialization
	void Start () {
        p1Control = gameObject.AddComponent<PlayerInputManager>();
        p1Control.assignedPlayer = PlayerIdentifier.P1;
        p2Control = gameObject.AddComponent<PlayerInputManager>();
        p2Control.assignedPlayer = PlayerIdentifier.P2;
        p3Control = gameObject.AddComponent<PlayerInputManager>();
        p3Control.assignedPlayer = PlayerIdentifier.P3;
        p4Control = gameObject.AddComponent<PlayerInputManager>();
        p4Control.assignedPlayer = PlayerIdentifier.P4;
    }
	
	// Update is called once per frame
	void Update () {
        if (p1Control.bStartPressed || p2Control.bStartPressed || p3Control.bStartPressed || p4Control.bStartPressed) {
            SoundManager.PlaySound("Confirm");
            SceneManager.LoadScene("MainMenu");
        }
	}
}
