﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{

    public static LevelManager instance = null;
    public static PlayerSpawn[] spawnList = null;

    public static LevelManager GetInstance()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<LevelManager>();
            if(instance == null)
            {
                GameObject newInstance = new GameObject("Level Manager");
                newInstance.AddComponent<LevelManager>();
                instance = FindObjectOfType<LevelManager>();
            }
            
        }

        return instance;
    }

    void Start ()
    {
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
        Application.targetFrameRate = 60;
        UpdateSpawnList();
        SoundManager.PlayFightBGM();
    }

    void Update () {
        if(GameManager.numberOfPlayersAlive <= 1 && GameManager.currentGameMode != "Race") {
            GameManager.numberOfPlayersAlive = 4; //just to be sure
            SoundManager.StopBGM();
            StartCoroutine(WaitForNextLevel());
        } else
        if (GameManager.numberOfPlayersAlive <= 0)
        {
            GameManager.numberOfPlayersAlive = 4; //just to be sure
            SoundManager.StopBGM();
            StartCoroutine(WaitForNextLevel());
        }
    }

    IEnumerator WaitForNextLevel ( ) {
        yield return new WaitForSeconds(2);
        GameManager.VerifyAlivePlayers();
        GameManager.NextLevel();
    }

    public void CallNextLevel()
    {
        StartCoroutine(DirectWaitForNextLevel());
    }

    IEnumerator DirectWaitForNextLevel()
    {
        yield return new WaitForSeconds(3);
        GameManager.VerifyAlivePlayers();
        GameManager.NextLevel();
    }

    void UpdateSpawnList ()
    {
        spawnList = FindObjectsOfType<PlayerSpawn>();
        int currentPlayer = 0;
        var currentPlayerID = PlayerIdentifier.P1;
        bool isActive = true;
        foreach (PlayerSpawn obj in spawnList)
        {
            switch (currentPlayer)
            {
                case 0:
                    currentPlayerID = PlayerIdentifier.P1;
                    isActive = GameManager.p1Active;
                    break;
                case 1:
                    currentPlayerID = PlayerIdentifier.P2;
                    isActive = GameManager.p2Active;
                    break;
                case 2:
                    currentPlayerID = PlayerIdentifier.P3;
                    isActive = GameManager.p3Active;
                    break;
                case 3:
                    currentPlayerID = PlayerIdentifier.P4;
                    isActive = GameManager.p4Active;
                    break;
                default:
                    currentPlayerID = PlayerIdentifier.AI;
                    break;
            }
            PlayerSpawn temp = obj.GetComponent<PlayerSpawn>();
            temp.UpdateAssignedController(currentPlayerID);
            temp.DisablePlayer(isActive);
            currentPlayer++;
        }
    }

}