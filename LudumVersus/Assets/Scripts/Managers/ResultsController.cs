﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ResultsController : MonoBehaviour {

    PlayerInputManager p1Control;
    PlayerInputManager p2Control;
    PlayerInputManager p3Control;
    PlayerInputManager p4Control;


    TextMesh WinningPlayer;
	// Use this for initialization
	void Start () {
        GameObject tempWinningPlayer = GameObject.Find("WinnerTitle");
        WinningPlayer = tempWinningPlayer.GetComponent<TextMesh>();

        p1Control = gameObject.AddComponent<PlayerInputManager>();
        p1Control.assignedPlayer = PlayerIdentifier.P1;
        p2Control = gameObject.AddComponent<PlayerInputManager>();
        p2Control.assignedPlayer = PlayerIdentifier.P2;
        p3Control = gameObject.AddComponent<PlayerInputManager>();
        p3Control.assignedPlayer = PlayerIdentifier.P3;
        p4Control = gameObject.AddComponent<PlayerInputManager>();
        p4Control.assignedPlayer = PlayerIdentifier.P4;

        WinningPlayer.text = GameManager.GetWinningPlayer();
    }

	// Update is called once per frame
	void Update () {
        if (p1Control.bStartPressed || p2Control.bStartPressed || p3Control.bStartPressed || p4Control.bStartPressed ||
            p1Control.bJumpPressed || p2Control.bJumpPressed || p3Control.bJumpPressed || p4Control.bJumpPressed) {
            SceneManager.LoadScene("PlayerSelect");

        }
    }
}
