﻿using UnityEngine;
using System.Collections;

public class WpnRock : BaseWeapon {

    PlayerInputManager currentInput;
    BasePlayer currentPlayer;

    public int ammo = 30;
    public int fireDelay = 8;
    public int currentDelay = 0;
    // Use this for initialization
    void Start ( ) {
        currentInput = GetComponent<PlayerInputManager>();
        currentPlayer = GetComponent<BasePlayer>();
    }

    // Update is called once per frame
    void Update ( ) {
        if (currentInput.bFirePressed && currentDelay <= 0) {
            GameObject tempRock = Instantiate(Resources.Load<GameObject>("Weapons/Throwable/WpnRock/RockTemplate"));
            tempRock.transform.position = currentPlayer.gameObject.transform.position + new Vector3(currentPlayer.currentDirection.x * 0.2f, currentPlayer.currentDirection.y * 0.2f - 0.1f, 0f);
            Rock rock = tempRock.GetComponent<Rock>();
            rock.SetTargetVelocity(new Vector2(currentPlayer.currentDirection.x, 2f) * 2f);
            ammo--;
            currentDelay = fireDelay;
            SoundManager.PlaySound("Throw");
            if (ammo == 0) {
                currentPlayer.DropWeapon();
            }
        }
        if (currentDelay > 0) {
            currentDelay--;
        }


    }
}
