﻿using UnityEngine;
using System.Collections;

public class Rock : MonoBehaviour {


    public int damage = 1;
    public PlayerIdentifier parentPlayer;
    public Rigidbody2D body;
    public Vector2 targetVelocity = new Vector2(0f, 0f);

    void Awake ( ) {
        body = GetComponent<Rigidbody2D>();
        body.velocity = targetVelocity;
        body.gravityScale = 0.7f;
    }

    void Start() {
        Destroy(gameObject, 6f);
    }

    public void SetTargetVelocity ( Vector2 _target ) {
        targetVelocity = _target;
        body.velocity = targetVelocity;
    }

    public void SetDamage ( int _damage ) {
        damage = _damage;
    }
    // Update is called once per frame
    void OnCollisionEnter2D ( Collision2D collision ) {
        if (collision.gameObject.layer == 9 && collision.relativeVelocity.magnitude > 3f) {
            BasePlayer targetHit = collision.gameObject.GetComponent<BasePlayer>();
            targetHit.health.TakeHit(damage);
            Destroy(gameObject, 1f);
            //return;
        }

    }
}
