﻿using UnityEngine;
using System.Collections;

public class BeachBall : MonoBehaviour {


    public int damage = 1;
    public PlayerIdentifier parentPlayer;
    public Rigidbody2D body;
    public Vector2 targetVelocity = new Vector2(0f, 0f);

    void Awake ( ) {
        body = GetComponent<Rigidbody2D>();
        body.velocity = targetVelocity;
        body.gravityScale = 0.2f;
    }

    void Start () {
        Destroy(gameObject, 12f);
    }

    public void SetTargetVelocity ( Vector2 _target ) {
        targetVelocity = _target;
        body.velocity = targetVelocity;
    }

    public void SetDamage ( int _damage ) {
        damage = _damage;
    }
    // Update is called once per frame
    void OnCollisionEnter2D ( Collision2D collision ) {
        
        if (collision.gameObject.layer == 9 && body.velocity.magnitude > 1.5f) {
            BasePlayer targetHit = collision.gameObject.GetComponent<BasePlayer>();
            Debug.Log(parentPlayer + " / "  + targetHit.currentInput.assignedPlayer);
            if (targetHit.currentInput.assignedPlayer != parentPlayer) {
                targetHit.health.TakeHit(damage);
                targetHit.PushOff(gameObject, 6f);
                Destroy(gameObject, 4f);
            }
        }
    }
}
