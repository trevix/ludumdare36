﻿using UnityEngine;
using System.Collections;


//this controls the weapon equipment
public class WpnBeachBall : BaseWeapon {

    PlayerInputManager currentInput;
    BasePlayer currentPlayer;

    public int ammo = 30;
    public int fireDelay = 60;
    public int currentDelay = 0;
    // Use this for initialization
    private BeachBall beachBallComponent;
    void Start ( ) {
        currentPlayer = gameObject.GetComponent<BasePlayer>();
        currentInput = currentPlayer.currentInput;
    }

    // Update is called once per frame
    void Update ( ) {
        if (currentInput.bFirePressed && currentDelay <= 0) {
            Debug.Log("BeachBall fire!");
            GameObject beachBallObject = Instantiate(Resources.Load<GameObject>("Weapons/Throwable/WpnBeachball/BeachBallTemplate"));
            beachBallObject.transform.position = currentPlayer.gameObject.transform.position + new Vector3(currentPlayer.currentDirection.x * 0.2f, currentPlayer.currentDirection.y * 0.2f - 0.1f, 0f);
            beachBallComponent = beachBallObject.GetComponent<BeachBall>();
            beachBallComponent.parentPlayer = currentInput.assignedPlayer;
            Debug.Log(beachBallComponent.parentPlayer);
            beachBallComponent.SetTargetVelocity(new Vector2(currentPlayer.currentDirection.x, 0.25f) * 4f);
            ammo--;
            currentDelay = fireDelay;
            StartCoroutine(ResetParentPlayer());
            SoundManager.PlaySound("Throw");
            if (ammo == 0) {
                currentPlayer.DropWeapon();
            }
        }
        if (currentDelay > 0) {
            currentDelay--;
        }
    }

    IEnumerator ResetParentPlayer ()
    {
        yield return new WaitForSeconds(1f);
        beachBallComponent.parentPlayer = PlayerIdentifier.AI;
    }
}
