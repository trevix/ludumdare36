﻿using UnityEngine;
using System.Collections;

public class WpnHand: BaseWeapon
{

    PlayerInputManager currentInput;
    BasePlayer currentPlayer;


    private int attackDuration = 10;
    private int currentAttackTime = 0;

    private int attackDelay = 15;
    private int currentAttackDelay = 0;

	// Use this for initialization
	void Start () {
        currentInput = gameObject.GetComponent<PlayerInputManager>();
        currentPlayer = gameObject.GetComponent<BasePlayer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (currentInput.bFirePressed && currentAttackDelay <= 0 && currentPlayer.isAlive)
        {
            currentPlayer.isAttacking = true;
            currentPlayer.animator.Play("Attack");
            //animator.Play("Attack");
            currentAttackTime = attackDuration;
            currentAttackDelay = attackDuration; //makes sure the delay will last the attack time + delay time
            GameObject tempMeleeHit = Instantiate(Resources.Load<GameObject>("Weapons/Melee/MeleeHit"));
            tempMeleeHit.transform.position = currentPlayer.gameObject.transform.position + new Vector3(currentPlayer.currentDirection.x * 0.1f, currentPlayer.currentDirection.y * 0.035f, 0f);
            Melee melee = tempMeleeHit.GetComponent<Melee>();
            melee.SetAttackDuration(attackDuration);
            melee.SetScale(0.22f);
            melee.SetParentId(currentPlayer.playerId);
            melee.SetForceDirection(currentPlayer.currentDirection*-1f);
            tempMeleeHit.transform.SetParent(currentPlayer.transform);
            SoundManager.PlaySound("Attack");
        }
	}

    void FixedUpdate () {
        currentAttackDelay--;
        if (currentAttackTime != 0) {
            currentAttackTime--;
            if(currentAttackTime == 0) {
                currentPlayer.isAttacking = false;
                currentAttackDelay = attackDelay;
            }
        }
    }
}
