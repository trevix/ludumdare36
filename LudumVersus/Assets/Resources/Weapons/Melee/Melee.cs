﻿using UnityEngine;
using System.Collections;

public class Melee : MonoBehaviour {


    public int damage = 1;
    public PlayerIdentifier parentPlayer;
    public int hitTime = 0;
    public float force = 2f;
    public Vector2 forceDirection = Vector2.up;
    public float scale = 1f;
    public BoxCollider2D collider;

    void Awake ( ) {
        collider = GetComponent<BoxCollider2D>();
    }

    public void SetDamage ( int _damage ) {
        damage = _damage;
    }

    public void SetScale (float _scale) {
        scale = _scale;
        collider.size = new Vector2(scale, scale);
    }

    public void SetParentId ( PlayerIdentifier _id ) {
        parentPlayer = _id;
    }

    public void SetForceDirection (Vector2 _forceDirection) {
        forceDirection = _forceDirection;
    }
    // Update is called once per frame
    void OnTriggerEnter2D ( Collider2D collision ) {
        if (collision.gameObject.layer == 9) {
            BasePlayer targetHit = collision.gameObject.GetComponent<BasePlayer>();
            targetHit.health.TakeHit(damage);
            targetHit.PushOff(gameObject, 2f);
        }
    }

    public void SetAttackDuration (int _time) {
        hitTime = _time;
    }

    void FixedUpdate () {
        hitTime--;
        if (hitTime < 0) {
            Destroy(gameObject);
        }
    }
}
